﻿using System.Web;
using System.Web.Mvc;

namespace MVC_DEVSET2015
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
